<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AttentionStatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AttentionStatesTable Test Case
 */
class AttentionStatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AttentionStatesTable
     */
    public $AttentionStates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.attention_states'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AttentionStates') ? [] : ['className' => AttentionStatesTable::class];
        $this->AttentionStates = TableRegistry::get('AttentionStates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AttentionStates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
