<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AttentionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AttentionsTable Test Case
 */
class AttentionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AttentionsTable
     */
    public $Attentions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.attentions',
        'app.patients',
        'app.medics',
        'app.states'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Attentions') ? [] : ['className' => AttentionsTable::class];
        $this->Attentions = TableRegistry::get('Attentions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Attentions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
