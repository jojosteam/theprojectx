<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
%>
<?php
/**
  * @var \<%= $namespace %>\View\AppView $this
  */
?>
<%
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}

%>
<?php 
$this->assign('content_title', '<%= $pluralHumanName %>'); 
$this->Breadcrumbs->add([
        ['title' => '<%= $pluralHumanName %>']
    ]);                
?>
<div class="row">
    <!-- Col de navegación -->
    <div class="col-md-3 col-md-push-9 special-box">
        <!--/.box -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> New <%= $singularHumanName %>',
                                ['action' => 'add'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                <%
                    $done = [];
                    foreach ($associations as $type => $data):
                        foreach ($data as $alias => $details):
                            if (!empty($details['navLink']) && $details['controller'] !== $this->name && !in_array($details['controller'], $done)):
                %>
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> List <%= $this->_pluralHumanName($alias) %>',
                                ['controller' => '<%= $details['controller'] %>', 'action' => 'index'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> New <%= $this->_singularHumanName($alias) %>', 
                                ['controller' => '<%= $details['controller'] %>', 'action' => 'add'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                <%
                                $done[] = $details['controller'];
                            endif;
                        endforeach;
                    endforeach;
                %>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!--/.box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <?= $this->Form->control('<%=$fields->toArray()[1]%>',[
                                    'label' => '<%=$fields->toArray()[1]%>',
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'index'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                        <?= $this->Form->end() ?>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="<%= $pluralVar %> index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">List New <%= $singularHumanName %></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                <% foreach ($fields as $field): %>
                                                <th scope="col"><?= $this->Paginator->sort('<%= $field %>', '<%= $field %>') ?></th>
                                <% endforeach; %>
                                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                                            </tr>
                                            <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
                                                <tr>
                                    <%        foreach ($fields as $field) {
                                                $isKey = false;
                                                if (!empty($associations['BelongsTo'])) {
                                                    foreach ($associations['BelongsTo'] as $alias => $details) {
                                                        if ($field === $details['foreignKey']) {
                                                            $isKey = true;
                                    %>
                                                    <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                                    <%
                                                            break;
                                                        }
                                                    }
                                                }
                                                if ($isKey !== true) {
                                                    if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
                                    %>
                                                    <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                                    <%
                                                    } else {
                                    %>
                                                    <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
                                    <%
                                                    }
                                                }
                                            }

                                            $pk = '$' . $singularVar . '->' . $primaryKey[0];
                                    %>
                                                    <td class="actions">
                                                        <?= $this->Html->link('<i class="fa fa-search"></i>', 
                                                            ['action' => 'view', <%= $pk %>],
                                                            ['class' => 'btn btn-box-tool', 'escape' => false]) 
                                                        ?>
                                                        <?= $this->Html->link('<i class="fa fa-pencil-square-o"></i>',
                                                            ['action' => 'edit', <%= $pk %>],
                                                            ['class' => 'btn btn-box-tool', 'escape' => false]) ?>
                                                        <?= $this->Form->postLink('<i  class="fa fa-trash"></i>', 
                                                            ['action' => 'delete', <%= $pk %>],
                                                            [
                                                                'confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>),
                                                                'class' => 'btn btn-box-tool', 
                                                                'escape' => false
                                                            ]) ?>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <?php echo $this->element('paginator'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>