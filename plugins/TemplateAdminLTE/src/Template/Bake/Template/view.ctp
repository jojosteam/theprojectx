<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
%>
<?php
/**
  * @var \<%= $namespace %>\View\AppView $this
  */
?>
<%
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
    ->map(function($field) use ($immediateAssociations) {
        foreach ($immediateAssociations as $alias => $details) {
            if ($field === $details['foreignKey']) {
                return [$field => $details];
            }
        }
    })
    ->filter()
    ->reduce(function($fields, $value) {
        return $fields + $value;
    }, []);

$groupedFields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    })
    ->groupBy(function($field) use ($schema, $associationFields) {
        $type = $schema->columnType($field);
        if (isset($associationFields[$field])) {
            return 'string';
        }
        if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
            return 'number';
        }
        if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
            return 'date';
        }
        return in_array($type, ['text', 'boolean']) ? $type : 'string';
    })
    ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>
<?php 
$this->assign('content_title', '<%= $pluralHumanName %>'); 
$this->Breadcrumbs->add([
        ['title' => '<%= $pluralHumanName %>', 'url' => ['controller' => '<%= $pluralVar %>', 'action' => 'index']],
        ['title' => '<%= $singularHumanName %> information']
    ]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <!-- Header del contenedor -->
                <h3 class="box-title">Información <%= $singularHumanName %></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <% if ($groupedFields['string']) : %>
                            <% foreach ($groupedFields['string'] as $field) : %>
                            <% if (isset($associationFields[$field])) :
                                        $details = $associationFields[$field];
                            %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('<%= Inflector::humanize($details['property']) %>') ?></label>
                                        <span><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></span>
                                        </div>
                                    </div>
                            <% else : %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('<%= Inflector::humanize($field) %>') ?></label>
                                        <span><?= h($<%= $singularVar %>-><%= $field %>) ?></span>
                                        </div>
                                    </div>
                            <% endif; %>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($associations['HasOne']) : %>
                                <%- foreach ($associations['HasOne'] as $alias => $details) : %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('<%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>') ?></label>
                                        <span><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></span>
                                        </div>
                                    </div>
                                <%- endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['number']) : %>
                            <% foreach ($groupedFields['number'] as $field) : %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('<%= Inflector::humanize($field) %>') ?></label>
                                        <span><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></span>
                                        </div>
                                    </div>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['date']) : %>
                            <% foreach ($groupedFields['date'] as $field) : %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></label>
                                        <span><?= h($<%= $singularVar %>-><%= $field %>) ?></span>
                                        </div>
                                    </div>
                            <% endforeach; %>
                            <% endif; %>
                            <% if ($groupedFields['boolean']) : %>
                            <% foreach ($groupedFields['boolean'] as $field) : %>
                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('<%= Inflector::humanize($field) %>') ?></label>
                                        <span><?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?></span>
                                        </div>
                                    </div>
                            <% endforeach; %>
                            <% endif; %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>