<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    });

if (isset($modelObject) && $modelObject->hasBehavior('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}
%>
<?php 
$this->assign('content_title', '<%= $pluralHumanName %>'); 
$this->Breadcrumbs->add([
        ['title' => '<%= $pluralHumanName %>', 'url' => ['controller' => '<%= $pluralVar %>', 'action' => 'index']],
        ['title' => '<%= !strpos($action, "add") ? "Update" : "New" %> <%= $singularHumanName %>']
    ]); 
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <!-- Acciones -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Navegación</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-book icon-min"></i> Volver',
                                ['controller' => '<%= $pluralVar %>', 'action' => 'index'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!--/.box -->
        <!-- /.box -->
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div class="box box-danger">
            <div class="box-header with-border">
            <% if (strpos($action, 'add') === false): %>
                <h3 class="box-title">Update <%= $singularHumanName %></h3>
            <% else:%>
                <h3 class="box-title">New <%= $singularHumanName %></h3>
            <% endif; %>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create($<%= $singularVar %>) ?>
                            <div class="row">
                    <%
                    foreach ($fields as $field) {
                        if (in_array($field, $primaryKey)) {
                            continue;
                        }
                        if (isset($keyFields[$field])) {
                            $fieldData = $schema->column($field);
                            if (!empty($fieldData['null'])) {
                    %>
                                <div class="col-md-4">
                                    <div class="form-group"><?php
                                        echo $this->Form->control('<%= $field %>', ['label' => '<%= $field %>', 'options' => $<%= $keyFields[$field] %>, 'empty' => true, 'class' => '']);?>
                                    </div>
                                </div>
                    <%
                            } else {
                    %>          
                                <div class="col-md-4">
                                    <div class="form-group"><?php
                                        echo $this->Form->control('<%= $field %>', ['label' => '<%= $field %>', 'options' => $<%= $keyFields[$field] %>,'class' => '']);?>
                                    </div>
                                </div>
                    <%
                            }
                            continue;
                        }
                        if (!in_array($field, ['created', 'modified', 'updated'])) {
                            $fieldData = $schema->column($field);
                            if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (!empty($fieldData['null']))) {
                    %>          <div class="col-md-4">
                                    <div class="form-group"><?php
                                        echo $this->Form->control('<%= $field %>', ['label' => '<%= $field %>', 'placeholder' => '<%= $field %>', 'empty' => true,'class' => 'form-control']);?>
                                    </div>
                                </div>
                    <%
                            } else {
                    %>          <div class="col-md-4">
                                    <div class="form-group"><?php
                                        echo $this->Form->control('<%= $field %>', ['label' => '<%= $field %>', 'placeholder' => '<%= $field %>', 'class' => 'form-control']);?>
                                    </div>
                                </div>
                    <%
                            }
                        }
                    } 
                    if (!empty($associations['BelongsToMany'])) {
                        foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
                    %>  <?php
                        echo $this->Form->control('<%= $assocData['property'] %>._ids', ['label' => '<%= $field %>', 'options' => $<%= $assocData['variable'] %>]);
                        ?>
                    <%
                        }
                    }
                    %>
                            </div>
                            <?= $this->Form->button(__('Submit'),['class' => 'btn btn-danger']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>