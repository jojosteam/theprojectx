<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Attention Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $attention_date
 * @property int $patient_id
 * @property int $medic_id
 * @property int $state_id
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Medic $medic
 * @property \App\Model\Entity\State $state
 */
class Attention extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true,
    ];

    public function getInfo(){
        $return = "Sin información";
        $attention_date = date_create(date("Y-m-d", strtotime($this->attention_date)));
        $today = date_create(date("Y-m-d"));

        $days = explode(" ", date_diff($attention_date, $today)->format('%R %a'));

        if($days[0] == "+"){
            if($days[1] == 2)
                $return = "<span title='Se debe confirmar la atención'>Faltan ".$days[1]." dias</span>";
            else
                $return = "<span>Faltan ".$days[1]." dias</span>";
        }

        return $return;
    }
}
