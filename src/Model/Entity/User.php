<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $rut
 * @property string $fullname
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property bool $gender
 * @property string $address
 * @property string $phone
 * @property string $specialty
 * @property \Cake\I18n\FrozenDate $contract_date
 * @property int $consult_price
 * @property string $password
 * @property int $role_id
 *
 * @property \App\Model\Entity\Role $role
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        '*' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    public function getPermission($controller = null, $action = null){
        if($this->role_id == 1 || $this->role_id == 5){
            return true;
        }
        elseif($this->role_id == 2){
            if($action == "statistics" || $action == "add")
                return false;
            return true;
        }
        elseif($this->role_id == 3){
            if($action == "statistics" || $action == "add")
                return false;
            return true;
        }
        elseif($this->role_id == 4){
            if($controller == "Attentions" && $action == "index")
                return true;
            return false;
        }
    }
}
