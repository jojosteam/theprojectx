<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Attentions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Patients
 * @property \Cake\ORM\Association\BelongsTo $Medics
 * @property \Cake\ORM\Association\BelongsTo $States
 *
 * @method \App\Model\Entity\Attention get($primaryKey, $options = [])
 * @method \App\Model\Entity\Attention newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Attention[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Attention|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Attention patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Attention[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Attention findOrCreate($search, callable $callback = null, $options = [])
 */
class AttentionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('attentions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Patients', [
            'className' => 'Users',
            'foreignKey' => 'patient_id',
            'joinType' => 'INNER'
        ])->setConditions(['Patients.role_id' => 4]);
        $this->belongsTo('Medics', [
            'className' => 'Users',
            'foreignKey' => 'medic_id',
            'joinType' => 'INNER'
        ])->setConditions(['Medics.role_id' => 5]);
        $this->belongsTo('States', [
            'className' => 'AttentionStates',
            'foreignKey' => 'state_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->add('medic_id', 'Search.Like', [
                'field' => ['medic_id']
            ])
            ->add('state_id', 'Search.Like', [
                'field' => ['state_id']
            ])
            ->add('specialty', 'Search.Callback',[
                'callback' => function($query, array $args, $type){
                    $likeValue = "%".$args['specialty']."%";
                    return $query->innerJoinWith('Medics', function($q) use ($likeValue) {
                        return $q->where(['Medics.specialty LIKE' => $likeValue]);
                    });
                }
            ])
            ->add('date_start', 'Search.Callback',[
                'callback' => function($query, $args, $filter) {
                    return $query->where(['Attentions.attention_date >=' => $args["date_start"]]);
                }
            ])
            ->add('date_finish', 'Search.Callback',[
                'callback' => function($query, $args, $filter) {
                    return $query->where(['Attentions.attention_date <=' => $args["date_finish"]]);
                }
            ])
            ->add('month', 'Search.Callback',[
                'callback' => function($query, $args, $filter) {
                    return $query->where(['month(Attentions.attention_date) =' => $args["month"]]);
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->dateTime('attention_date')
            ->requirePresence('attention_date')
            ->notEmpty('attention_date', 'Este campo no puede estar vacío.');

        $validator
            ->notEmpty('patient_id', 'Este campo no puede estar vacío.');

        $validator
            ->notEmpty('medic_id', 'Este campo no puede estar vacío.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['patient_id'], 'Patients'));
        $rules->add($rules->existsIn(['medic_id'], 'Medics'));
        $rules->add($rules->existsIn(['state_id'], 'States'));

        return $rules;
    }
}
