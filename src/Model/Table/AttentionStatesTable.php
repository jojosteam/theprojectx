<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttentionStates Model
 *
 * @method \App\Model\Entity\AttentionState get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttentionState newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttentionState[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttentionState|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttentionState patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttentionState[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttentionState findOrCreate($search, callable $callback = null, $options = [])
 */
class AttentionStatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attention_states');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('name')
            ->maxLength('name')
            ->requirePresence('name')
            ->notEmpty('name');

        $validator
            ->scalar('code')
            ->maxLength('code')
            ->requirePresence('code')
            ->notEmpty('code');

        return $validator;
    }
}
