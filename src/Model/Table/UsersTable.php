<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Attentions', [
            'foreignKey' => 'patient_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('rut')
            ->maxLength('rut',12)
            ->requirePresence('rut')
            ->notEmpty('rut');

        $validator
            ->scalar('fullname')
            ->maxLength('fullname',100)
            ->requirePresence('fullname')
            ->notEmpty('fullname');

        $validator
            ->date('birthdate')
            ->allowEmpty('birthdate');

        $validator
            ->boolean('gender')
            ->allowEmpty('gender');

        $validator
            ->scalar('address')
            ->maxLength('address',255)
            ->allowEmpty('address');

        $validator
            ->scalar('phone')
            ->maxLength('phone',15)
            ->allowEmpty('phone');

        $validator
            ->scalar('specialty')
            ->maxLength('specialty',100)
            ->allowEmpty('specialty');

        $validator
            ->date('contract_date')
            ->allowEmpty('contract_date');

        $validator
            ->integer('consult_price')
            ->allowEmpty('consult_price');

        $validator
            ->scalar('password')
            ->maxLength('password',255)
            ->requirePresence('password')
            ->notEmpty('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->isUnique(['rut'], 'El rut ingresado ya existe en el sistema. Por favor, intente nuevamente.'));

        return $rules;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->select(['id', 'rut', 'password', 'fullname', 'birthdate', 'gender', 'role_id']);

        return $query;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options)
    {
        $hasher = new DefaultPasswordHasher;
        $entity->password = $hasher->hash($entity->password);
        return true;
    }
}
