<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

/**
 * UpdateState shell command.
 */
class UpdateStateShell extends Shell
{

    protected $Attentions;

    private function init(){
        $this->Attentions = TableRegistry::get('Attentions');
    }

    public function main(){
        $this->init();

        $attentions = $this->Attentions->find("all", [
            "conditions" => [
                "attention_date <=" => date("Y-m-d", strtotime(date("Y-m-d")." +1 day"))
            ]
        ]);

        foreach($attentions as $key => $attention){
            $attention_date = date_create(date("Y-m-d", strtotime($attention->attention_date)));
            $today = date_create(date("Y-m-d"));

            $days = explode(" ", date_diff($attention_date, $today)->format('%R %a'));

            if($attention->state_id == 1){
                if($days[1] == 1 && $days[0] == "+")
                    $attention->state_id = 3;
            }
            elseif($attention->state_id == 2){
                if($days[1] == 1 && $days[0] == "-")
                    $attention->state_id = 4;
            }

            $this->Attentions->save($attention);
        }
    }
}
