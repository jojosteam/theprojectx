<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('rut', ['label' => 'RUT',  'class' => 'rut form-control']);?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('fullname', ['label' => 'Nombre completo', 'class' => 'form-control']);?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('password', ['label' => 'Contraseña', 'class' => 'form-control']);?>
                </div>
            </div>
        </div>
        <?php 
            if(isset($medic)): ?>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('specialty', ['label' => 'Especialidad', 'class' => 'form-control']);?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('contract_date', ['type' => 'text','readonly'=>'readonly','label' => 'Fecha de contratación', 'empty' => true,'style' => 'cursor:pointer','class' => 'form-control datepicker']);?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('consult_price', ['min'=>1,'label' => 'Precio de Consulta', 'class' => 'form-control']);?>
                        </div>
                    </div>
                </div><?php
            else: ?>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('birthdate', ['type' => 'text','readonly'=>'readonly','label' => 'Fecha de nacimiento', 'empty' => true,'style' => 'cursor:pointer','class' => 'form-control datepicker']);?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            $options = [1 => 'Masculino', 2 => 'Femenino'];
                            echo $this->Form->control('gender', ['type' => 'select', 'class' => 'form-control', 'options' => $options, 'label' => 'Sexo']);?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('address', ['label' => 'Dirección', 'class' => 'form-control']);?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group"><?php
                            echo $this->Form->control('phone', ['label' => 'Telefono','class' => 'form-control']);?>
                        </div>
                    </div>
                </div><?php
            endif;
        ?>
        <!-- <div class="col-md-4">
            <div class="form-group"><?php
                //echo $this->Form->control('role_id', ['label' => 'Rol', 'options' => $roles,'class' => 'form-control']);?>
            </div>
        </div> -->
    </div>
</div>