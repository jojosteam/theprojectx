<div class="col-md-12">
    <nav>
        <ul class="pager">
            <div class="pager-numbers hidden-xs">
                <?= $this->Paginator->first() ?>
                <?= $this->Paginator->prev() ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next() ?>
                <?= $this->Paginator->last() ?>
            </div>
        </ul>
    </nav>
</div>