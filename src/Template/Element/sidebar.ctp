<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/userDefaultIcon.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Semana <?= date("W") ?></p>
                <p><a href="#"><?=$currentUser['username']?></a></p>
                <!-- <span>correogenerico@123.cl</span> -->
            </div>
        </div>
        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Sidebar</li><?
            foreach($sidebar as $li):
                $active = '';
                $validate = 0;
                $li_c = 0;
                $submenu = $li["submenu"] !== false ? true : false;                

                if($li['current']['controller'] == false){
                    $setActive = array();
                    $li_ct = 0;
                    foreach($li["submenu"] as $key => $submenu){
                        $li["submenu"][$key]["active"] = '';
                        $li["submenu"][$key]["permission"] = true;
                        $li["submenu"][$key]["parent"] = true;
                        if($submenu["current"] != false){
                            $permission = $currentUser->getPermission($submenu["current"]["controller"], $submenu["current"]["action"]);
                            $li["submenu"][$key]["active"] = $submenu["current"]["controller"] == $this->request->params["controller"] && $submenu["current"]["action"] == $this->request->params["action"] ? "active" : "";
                            $li["submenu"][$key]["permission"] = $permission;
                            if(!$permission)
                                $li_c++;
                        }
                        $li["submenu"][$key]["active"] .= isset($submenu['submenu']) && !empty($submenu['submenu']) ? " treeview" : "";

                        $setActive[] = $submenu["current"]["controller"];
                        $setActiveSecond = array();
                        if(isset($submenu['submenu']) && !empty($submenu['submenu'])){
                            $submenu_c = 0;

                            foreach($submenu['submenu'] as $key_submenu => $secondSubmenu){
                                $permission = $currentUser->getPermission($secondSubmenu['current']['controller'], $secondSubmenu['current']['action']);
                                if(!$permission)
                                    $submenu_c++;

                                $li["submenu"][$key]["submenu"][$key_submenu]["active"] = "";
                                $li["submenu"][$key]["submenu"][$key_submenu]["permission"] = $permission;
                                if(($this->request->params["controller"] == $secondSubmenu['current']['controller']) && ($this->request->params["action"] == $secondSubmenu['current']['action'])){
                                    $li["submenu"][$key]["submenu"][$key_submenu]["active"] = "active";
                                    $li["submenu"][$key]["active"] = "active";
                                    $validate = 1;
                                }
                                /*elseif($this->request->params["controller"] == $secondSubmenu['current']['controller']){
                                    $li["submenu"][$key]["active"] = "active";
                                    $validate = 1;
                                }*/
                            }

                            if($submenu_c == count($submenu['submenu'])){
                                $li["submenu"][$key]["parent"] = false;
                                $li_c++;
                            }
                        }
                        $li_ct++;
                    }
                    if(!$validate){
                        $active = in_array($this->request->params["controller"], $setActive) ? "active" : "";
                    }
                    else{
                        $active = "active";
                    }
                }else{
                    $active = $li["current"]["controller"] == $this->request->params["controller"] ? "active" : "";
                }

                $url = $li["current"] !== false ? $li["current"] : 'javascript:void(0)';
                $ico = $li["ico"] !== false ? $li["ico"] : '';
                
                if($li["submenu"] === false):?>
                    <li class="<?=$active?>">
                        <?=$this->Html->link('<i class="fa '.$ico.'"></i><span>'.$li["name"].'</span>', $url, ["escape" => false]);?>
                    </li>
                    <? 
                else:
                    if($li_c != $li_ct || $li_ct == 0):?>
                        <li class="treeview <?=$active?>">
                            <?=$this->Html->link('<i class="fa '.$ico.'"></i><span>'.$li["name"].'</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>', $url, ["escape" => false])?>
                            <ul class="treeview-menu"><?
                                foreach($li["submenu"] as $key => $submenu):
                                    $url = $submenu["current"] !== false ? $submenu["current"] : "javascript:void(0)";
                                    $arrow_submenu = "";
                                    if(isset($submenu['submenu']) && !empty($submenu['submenu']))
                                        $arrow_submenu = '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                                    if($submenu["permission"] && $li["submenu"][$key]["parent"]):
                                        $secondSubmenuActive = false;
                                        if(isset($submenu['submenu']) && !empty($submenu['submenu'])){
                                            foreach($submenu['submenu'] as $secondSubmenu){
                                                if($secondSubmenu["active"] == "active"){
                                                    $secondSubmenuActive = true;
                                                    break;
                                                }
                                            }
                                        }?>
                                        <li class="<?=$secondSubmenuActive && $this->request->params["controller"] == $submenu["current"]["controller"] ? "active" : $submenu["active"]?>">
                                            <?=$this->Html->link('<i class="fa fa-circle-o"></i>'.$submenu["name"].$arrow_submenu, $url, ["escape" => false])?>
                                            <?
                                            if(isset($submenu['submenu']) && !empty($submenu['submenu'])):?>
                                            <ul class="treeview-menu"><?
                                                foreach($submenu['submenu'] as $secondSubmenu): 
                                                    $url = $secondSubmenu['current'] !== false ? $secondSubmenu['current'] : "javascript:void(0)";
                                                    if($secondSubmenu["permission"]):
                                                    ?>
                                                    <li class="<?=$secondSubmenu["active"]?>">
                                                        <?=$this->Html->link('<i class="fa fa-circle-o"></i>'.$secondSubmenu["name"], $url, ["escape" => false]);?>
                                                    </li><?
                                                    endif;  
                                                endforeach;?>
                                            </ul>
                                            <?
                                            endif;?>
                                        </li><?
                                    endif;
                                endforeach;?>
                            </ul>
                        </li><?
                    endif;
                endif;?>
            <? endforeach;?>
        </ul>
    </section>
</aside>