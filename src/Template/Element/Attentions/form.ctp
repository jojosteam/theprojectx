<div class="col-md-12">
    <?= $this->Form->create($attention) ?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('attention_date', ['label' => 'Fecha de atención', 'placeholder' => 'Fecha de atención', 'class' => 'form-control datetimepicker', 'type' => 'text', 'empty' => 'Seleccione']);?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('patient_id', ['label' => 'Paciente', 'placeholder' => 'patient_id', 'class' => 'form-control', 'empty' => 'Seleccione']);?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('medic_id', ['label' => 'Médico', 'placeholder' => 'medic_id', 'class' => 'form-control', 'empty' => 'Seleccione']);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group"><?php
                    echo $this->Form->control('state_id', ['label' => 'Estado', 'placeholder' => 'state_id', 'class' => 'form-control']);?>
                </div>
            </div>
        </div>
        <?= $this->Form->button(__('Guardar'),['class' => 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>