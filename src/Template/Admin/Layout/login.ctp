<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Acceso administrador</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/css/AdminLTE.min.css">
      <!--Estilos propios-->
      <!--link rel="stylesheet" href="/css/style.css"-->
       <!-- ./wrapper -->
      
      <!-- jQuery 3.1.1 -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="/js/bootstrap.min.js"></script>
      <!-- <script src="/js/jquery-2.2.3.min.js"></script>
      <script src="/js/select2.full.min.js"></script> -->
      <!-- Bootstrap 3.3.6 -->
      <script src="/js/bootstrap.min.js"></script>
      <script src="/js/jquery-rut.min.js"></script>
    </head>
    <body class="hold-transition login-page">
      <?= $this->fetch('content') ?>
    </body>
</html>