<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- <title>AdminLTE 2 | Dashboard</title> -->
      <title>Hospital</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.6 -->
      <link rel="stylesheet" href="/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/css/AdminLTE.min.css">
      <!--Estilos propios-->
      <!--link rel="stylesheet" href="/css/select2.min.css"-->
      <link rel="stylesheet" href="/css/style_admin.css">
      <link rel="stylesheet" href="/css/jquery-ui.min.css">
       <!-- ./wrapper -->
      
      <!-- jQuery 3.3.1 -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <!-- FastClick -->
      <script src="/js/fastclick.min.js"></script>
      <!-- AdminLTE App -->
      <script src="/js/app.min.js"></script>
      <!-- Moment -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
      <!-- ChartJS 1.0.1 -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
      <!-- Dev Requirements -->
      <?=$this->Html->script(['jquery-ui-1.12.min', 'select2.full.min', 'global', 'tableHeadFixer', 'bootstrap-select', 'bootstrap-datetimepicker.min', 'bootstrap-datetimepicker.es', 'jquery-rut.min']);?>
      <?=$this->Html->css(['jquery-ui-1.12.min', 'bootstrap-select', 'bootstrap-datetimepicker.min']);?>
   </head>
   <body class="skin-blue sidebar-mini">
      <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <?= $this->Html->link('<span class="logo-mini">EFQN</span>
                                   <span class="logo-lg"><img class="logo-img" src="/img/logo_admin.jpg"></span>',
                ['controller' => 'news', 'action' => 'index'],
                ['class' => 'logo', 'escape' => false]); ?>
            
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle sm-only" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!-- <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Desplegable</li>
                            </ul>
                        </li> -->
                        <!-- Notifications: style can be found in dropdown.less -->
                        <!-- <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 10 notifications</li>
                            </ul>
                        </li> -->
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!-- <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have 9 tasks</li>
                            </ul>
                        </li> -->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="/admin/users/logout" class="dropdown-toggle" data-toggle="dropdown-menu">
                              <img src="/img/userDefaultIcon.png" class="user-image" alt="User Image">
                              <span class="hidden-xs">Cerrar sesión</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="/img/userDefaultIcon.png" class="img-circle" alt="User Image">
                                    <p style="color:black;">
                                        <?=$currentUser["fullname"]?>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </div>
                                </li> -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?= $this->Html->link('Perfil',
                                            ['controller' => 'users', 'action' => 'profile'],
                                            ['class' => 'btn btn-default btn-flat']); ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= $this->Form->postLink('Cerrar sesión',
                                            ['controller' => 'session', 'action' => 'logout'],
                                            ['class' => 'btn btn-default btn-flat']); ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
         <!-- Left side column. contains the logo and sidebar -->
        <?=$this->element('sidebar')?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <?= $this->Flash->render() ?>
         
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1><?= h($this->fetch('content_title')) ?></h1>
                  
                  <!-- Breadcrumbs -->
                  <?php 
                  $this->Breadcrumbs->templates([
                    'wrapper' => '<ol class="breadcrumb"{{attrs}}>{{content}}</ol>',
                    'item' => '<li><a href="{{url}}"{{innerAttrs}}>{{title}}</a></li>{{separator}}',
                    'itemWithoutLink' => '<li class="active"><span>{{title}}</span></li>{{separator}}',
                    'separator' => '/'
                  ]);
                  echo $this->Breadcrumbs->render();
                  ?>

            </section>

            <!-- Main content -->
            <section class="content"> 
                <?= $this->fetch('content') ?>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         <footer class="main-footer">
            <div class="pull-right hidden-xs">
               <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; <?=date("Y")?> <a href="#">sad</a>.</strong> Todos los derechos reservados.
         </footer>
      </div>
   </body>
</html>