<?php 
$this->assign('content_title', 'Atenciones'); 
$this->Breadcrumbs->add([
        ['title' => 'Atenciones']
    ]);                
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('date_start',[
                                                'label' => 'Fecha inicio',
                                                'class' => 'form-control datepicker',
                                                'value' => isset($this->request->query['date_start'])?$this->request->query['date_start']:""
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('date_finish',[
                                                'label' => 'Fecha final',
                                                'class' => 'form-control datepicker',
                                                'value' => isset($this->request->query['date_finish'])?$this->request->query['date_finish']:""
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('medic_id',[
                                    'label' => 'Médico',
                                    'type' => 'select',
                                    'options' => $medics,
                                    'value' => isset($this->request->query['medic_id'])?$this->request->query['medic_id']:"",
                                    'empty' => 'Seleccione un medico',
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('specialty',[
                                    'label' => 'Especialidad',
                                    'type' => 'text',
                                    'value' => isset($this->request->query['specialty'])?$this->request->query['specialty']:"",
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('month',[
                                    'type' => 'select',
                                    'label' => 'Mes',
                                    'options' => $month,
                                    'empty' => 'Seleccione un mes',
                                    'value' => isset($this->request->query['month'])?$this->request->query['month']:"",
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $this->Form->control('state_id', [
                                        'label' => 'Estados',
                                        'options' => $states,
                                        'empty' => 'Seleccione',
                                        'value' => isset($this->request->query['state_id'])?$this->request->query['state_id']:"",
                                        'class'=>'form-control',
                                        'id' => 'role_user']) 
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'statistics'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                            <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="attentions index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estadisticas</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                <th scope="col">Estado</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Total</th>
                                            <?php foreach ($attentions as $attention): ?>
                                                <tr>
                                                    <td><?= $attention->state->name ?></td>
                                                    <td><?= $this->Number->format($attention->total) ?></td>
                                                    <td><?= $this->Number->format($attention->valorTotal) ?></td>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>