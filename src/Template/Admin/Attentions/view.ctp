<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Attentions'); 
$this->Breadcrumbs->add([
        ['title' => 'Attentions', 'url' => ['controller' => 'attentions', 'action' => 'index']],
        ['title' => 'Attention information']
    ]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <!-- Header del contenedor -->
                <h3 class="box-title">Información Attention</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                                                                                                                                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Id') ?></label>
                                        <span><?= $this->Number->format($attention->id) ?></span>
                                        </div>
                                    </div>
                                                                <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Patient Id') ?></label>
                                        <span><?= $this->Number->format($attention->patient_id) ?></span>
                                        </div>
                                    </div>
                                                                <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Medic Id') ?></label>
                                        <span><?= $this->Number->format($attention->medic_id) ?></span>
                                        </div>
                                    </div>
                                                                <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('State Id') ?></label>
                                        <span><?= $this->Number->format($attention->state_id) ?></span>
                                        </div>
                                    </div>
                                                                                                                                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Attention Date') ?></label>
                                        <span><?= h($attention->attention_date) ?></span>
                                        </div>
                                    </div>
                                                                                                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>