<?php 
$this->assign('content_title', 'Atenciones'); 
$this->Breadcrumbs->add([
        ['title' => 'Atenciones']
    ]);                
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <? if($currentUser->role_id != 3 && $currentUser->role_id != 4): ?>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Nueva atención',
                                    ['action' => 'add'], 
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif;?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('date_start',[
                                                'label' => 'Fecha inicio',
                                                'class' => 'form-control datepicker',
                                                'value' => isset($this->request->query['date_start'])?$this->request->query['date_start']:""
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('date_finish',[
                                                'label' => 'Fecha final',
                                                'class' => 'form-control datepicker',
                                                'value' => isset($this->request->query['date_finish'])?$this->request->query['date_finish']:""
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('medic_id',[
                                    'label' => 'Médico',
                                    'type' => 'select',
                                    'options' => $medics,
                                    'value' => isset($this->request->query['medic_id'])?$this->request->query['medic_id']:"",
                                    'empty' => 'Seleccione un medico',
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('specialty',[
                                    'label' => 'Especialidad',
                                    'type' => 'text',
                                    'value' => isset($this->request->query['specialty'])?$this->request->query['specialty']:"",
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $this->Form->control('state_id', [
                                        'label' => 'Estados',
                                        'options' => $states,
                                        'empty' => 'Seleccione',
                                        'value' => isset($this->request->query['state_id'])?$this->request->query['state_id']:"",
                                        'class'=>'form-control',
                                        'id' => 'role_user']) 
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'index'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                            <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="attentions index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de atenciones</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                <th scope="col"><?= $this->Paginator->sort('id', 'ID') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('attention_date', 'Fecha de atención') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('patient_id', 'Paciente') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('medic_id', 'Médico') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('state_id', 'Estado') ?></th>
                                                <th scope="col"><?= __("Info") ?></th>
                                                <? if($currentUser->role_id != 3 && $currentUser->role_id != 4): ?>
                                                    <th scope="col" class="actions"><?= __('Acciones') ?></th>
                                                <? endif;?>
                                            </tr>
                                            <?php foreach ($attentions as $attention): ?>
                                                <tr>
                                                    <td><?= $this->Number->format($attention->id) ?></td>
                                                    <td><?= h($attention->attention_date->format("d-m-Y H:i")) ?></td>
                                                    <td><?= $attention->patient->fullname ?></td>
                                                    <td><?= $attention->medic->fullname ?></td>
                                                    <td><?= $attention->state->name ?></td>
                                                    <td><?= $attention->getInfo() ?></td>
                                                    <? if($currentUser->role_id != 3 && $currentUser->role_id != 4): ?>
                                                        <td class="actions">
                                                            <?/*= $this->Html->link('<i class="fa fa-pencil-square-o"></i>',
                                                                ['action' => 'edit', $attention->id],
                                                                ['class' => 'btn btn-box-tool', 'escape' => false, 'title' => 'Editar']) */?>
                                                            <?= $this->Form->postLink('<i  class="fa fa-trash"></i>', 
                                                                ['action' => 'delete', $attention->id],
                                                                [
                                                                    'confirm' => __('Estas seguro de eliminar # {0}?', $attention->id),
                                                                    'class' => 'btn btn-box-tool', 
                                                                    'escape' => false,
                                                                    'title' => 'Eliminar'
                                                                ]) ?>
                                                            <?= $attention->state_id != 5 && $attention->state_id != 3 && $attention->state_id != 4 ? $this->Html->link('<i class="fa fa-ban"></i>',
                                                                ['action' => 'changeState', $attention->id, 3],
                                                                [
                                                                    'confirm' => __('Estas seguro de anular # {0}?', $attention->id),
                                                                    'class' => 'btn btn-box-tool',
                                                                    'escape' => false,
                                                                    'title' => 'Anular'
                                                                ]) : ""?>

                                                            <?= $attention->state_id == 2 ? $this->Html->link('<i class="fa fa-times"></i>',
                                                                ['action' => 'changeState', $attention->id, 4],
                                                                [
                                                                    'confirm' => __('Estas seguro de pasar a perdida # {0}?', $attention->id),
                                                                    'class' => 'btn btn-box-tool',
                                                                    'escape' => false,
                                                                    'title' => 'Pasar a perdida'
                                                                ]) : ""?>

                                                            <?= $attention->state_id == 2 ? $this->Html->link('<i class="fa fa-check"></i>',
                                                                ['action' => 'changeState', $attention->id, 5],
                                                                ['class' => 'btn btn-box-tool', 'escape' => false, 'title' => 'Terminar']) : ""?>
                                                        </td>
                                                    <? endif;?>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <?php echo $this->element('paginator'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>