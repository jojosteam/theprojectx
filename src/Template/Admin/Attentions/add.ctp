<?php 
$this->assign('content_title', 'Atenciones'); 
$this->Breadcrumbs->add([
        ['title' => 'Atenciones', 'url' => ['controller' => 'attentions', 'action' => 'index']],
        ['title' => 'Agregar atención']
    ]); 
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Navegación</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-book icon-min"></i> Volver',
                                ['controller' => 'attentions', 'action' => 'index'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Agregar atención</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <?=$this->element("Attentions/form")?>
                </div>
            </div>
        </div>
    </div>
</div>