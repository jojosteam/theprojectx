<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Attention States'); 
$this->Breadcrumbs->add([
        ['title' => 'Attention States', 'url' => ['controller' => 'attentionStates', 'action' => 'index']],
        ['title' => 'Attention State information']
    ]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <!-- Header del contenedor -->
                <h3 class="box-title">Información Attention State</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Name') ?></label>
                                        <span><?= h($attentionState->name) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Code') ?></label>
                                        <span><?= h($attentionState->code) ?></span>
                                        </div>
                                    </div>
                                                                                                                                                                                                            <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Id') ?></label>
                                        <span><?= $this->Number->format($attentionState->id) ?></span>
                                        </div>
                                    </div>
                                                                                                                                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>