<?php 
$this->assign('content_title', 'Agregar '.$title); 
$this->Breadcrumbs->add([
        ['title' => 'Listado de usuario'],
        ['title' => 'Agregar '.$title]
    ]); 
?>
<div class="row">
    <?= $this->Form->create($user) ?>
    <div class="col-md-3 col-md-push-9 special-box">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Navegación</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-book icon-min"></i> Volver',
                                ['controller' => 'users', 'action' => 'index'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                            <?= $this->Form->button(__('<i class="fa fa-floppy-o fa-side"></i>Guardar'),['class' => 'btn btn-danger option']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div class="box box-danger">
            <div class="box-body">
                <div class="row">
                    <?=$this->element("Users/form")?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end()?> 
</div>