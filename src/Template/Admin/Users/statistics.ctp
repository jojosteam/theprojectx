<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Pacientes'); 
$this->Breadcrumbs->add([
        ['title' => 'Estadísticas de Pacientes']
    ]);                
?>
<div class="row">
    <!-- Col de navegación -->
    <div class="col-md-3 col-md-push-9 special-box">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <?= $this->Form->control('age_range',[
                                    'label' => 'Rango etario',
                                    'empty' => 'Seleccione',
                                    'options' => [
                                        1 => '0-13',
                                        2 => '14-26',
                                        3 => '27-59',
                                        4 => '60 +'
                                    ],
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('gender',[
                                    'label' => 'Sexo',
                                    'empty' => 'Seleccione',
                                    'options' => [
                                        1 => 'Masculino',
                                        2 => 'Femenino',
                                    ],
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('attentions_quantity',[
                                    'label' => 'Cantidad de atenciones',
                                    'type' => 'number',
                                    'min' => 1,
                                    'class' => 'rut form-control'])
                                ?>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'statistics'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                        <?= $this->Form->end() ?>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="users index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estadísticas de los pacientes</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                <th scope="col">ID</th>
                                                <th scope="col">RUT</th>
                                                <th scope="col">Nombre completo</th>
                                                <th scope="col">Edad</th>
                                                <th scope="col">Cantidad de atenciones</th>
                                            </tr>
                                            <?php foreach ($users as $user): ?>
                                                <tr>
                                                    <td><?= $this->Number->format($user->id) ?></td>
                                                    <td><?= h($user->rut) ?></td>
                                                    <td><?= h($user->fullname) ?></td>
                                                    <td><?= h($user->age) ?></td>
                                                    <td><?= h($user->attentions_quantity) ?></td>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>