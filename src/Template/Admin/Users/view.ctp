<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Users'); 
$this->Breadcrumbs->add([
        ['title' => 'Users', 'url' => ['controller' => 'users', 'action' => 'index']],
        ['title' => 'User information']
    ]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <!-- Header del contenedor -->
                <h3 class="box-title">Información User</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Rut') ?></label>
                                        <span><?= h($user->rut) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Fullname') ?></label>
                                        <span><?= h($user->fullname) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Address') ?></label>
                                        <span><?= h($user->address) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Phone') ?></label>
                                        <span><?= h($user->phone) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Specialty') ?></label>
                                        <span><?= h($user->specialty) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Password') ?></label>
                                        <span><?= h($user->password) ?></span>
                                        </div>
                                    </div>
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Role') ?></label>
                                        <span><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></span>
                                        </div>
                                    </div>
                                                                                                                                                                                                            <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Id') ?></label>
                                        <span><?= $this->Number->format($user->id) ?></span>
                                        </div>
                                    </div>
                                                                <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Consult Price') ?></label>
                                        <span><?= $this->Number->format($user->consult_price) ?></span>
                                        </div>
                                    </div>
                                                                                                                                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Birthdate') ?></label>
                                        <span><?= h($user->birthdate) ?></span>
                                        </div>
                                    </div>
                                                                <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Contract Date') ?></label>
                                        <span><?= h($user->contract_date) ?></span>
                                        </div>
                                    </div>
                                                                                                                                                    <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Gender') ?></label>
                                        <span><?= $user->gender ? __('Yes') : __('No'); ?></span>
                                        </div>
                                    </div>
                                                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>