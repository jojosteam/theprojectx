<div class="login-box">
    <div class="login-logo">
        Hospital
    </div>
    <div class="row">
        <?= $this->Flash->render('auth')?>
        <?= $this->Flash->render() ?>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Inicie sesión para comenzar</p>
        <?= $this->Form->create() ?>
            <div class="form-group has-feedback">
                <?= $this->Form->input('rut',[
            		'class'=>'form-control',
            		'placeholder'=>'RUT',
            		'label'=>false,
            		'required',
                    'id' => 'rut-field'
            	]) ?>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $this->Form->input('password',[
            		'class'=>'form-control',
            		'placeholder'=>'Contraseña',
            		'label'=>false,
            		'required']) 
            	?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4 pull-right">
                    <?= $this->Form->button('Enviar',[
                		'class'=>'btn btn-primary btn-block btn-flat'
                	])?>
                </div>
            </div>
            <script>
                $(document).ready(function(){
                    $('#rut-field').rut();
                });
            </script>
            <br>
    </div>
</div>
