<?php 
$this->assign('content_title', 'Usuarios'); 
$this->Breadcrumbs->add([
        ['title' => 'Usuarios']
    ]);                
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <? if($currentUser->role_id != 2): ?>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <?= $this->Html->link('Agregar nuevo médico',
                                    ['action' => 'add', 'medic'], 
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Html->link('Agregar nuevo paciente',
                                    ['action' => 'add', 'patient'], 
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Html->link('Agregar nuevo/a secretario/a',
                                    ['action' => 'add', 'secretarian'], 
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Html->link('Agregar nuevo director',
                                    ['action' => 'add', 'director'], 
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create(null, ['type' => 'get']) ?>
                            <div class="form-group">
                                <?= $this->Form->control('rut',[
                                    'label' => 'RUT',
                                    'class' => 'rut form-control',
                                    'value' => isset($this->request->query['rut'])?$this->request->query['rut']:"",
                                ])
                                ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $this->Form->control('role_id', [
                                        'label' => 'Rol',
                                        'options' => $roles,
                                        'empty' => 'Seleccione',
                                        'value' => isset($this->request->query['role_id'])?$this->request->query['role_id']:"",
                                        'class'=>'form-control',
                                        'id' => 'role_user']) 
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'index'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="users index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de usuarios</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                <th scope="col"><?= $this->Paginator->sort('id', 'ID') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('rut', 'RUT') ?></th>
                                                <th scope="col"><?= $this->Paginator->sort('fullname', 'Nombre completo') ?></th>
                                                <th scope="col">Rol</th><?php
                                                if($currentUser->role_id != 3 && $currentUser->role_id != 2): ?>
                                                    <th scope="col" class="actions"><?= __('Acciones') ?></th><?php
                                                endif; ?>
                                            </tr>
                                            <?php foreach ($users as $user): ?>
                                                <tr>
                                                    <td><?= $this->Number->format($user->id) ?></td>
                                                    <td><?= h($user->rut) ?></td>
                                                    <td><?= h($user->fullname) ?></td>
                                                    <td><?=$roles[$user->role_id]?></td><?php
                                                    if($currentUser->role_id != 3 && $currentUser->role_id != 2): ?>
                                                        <td class="actions">
                                                            <?= $this->Html->link('<i class="fa fa-search"></i>', 
                                                                ['action' => 'view', $user->id],
                                                                ['class' => 'btn btn-box-tool', 'escape' => false]) 
                                                            ?>
                                                            <?= $this->Html->link('<i class="fa fa-pencil-square-o"></i>',
                                                                ['action' => 'edit', $user->id],
                                                                ['class' => 'btn btn-box-tool', 'escape' => false]) ?>
                                                            <?= $this->Form->postLink('<i  class="fa fa-trash"></i>', 
                                                                ['action' => 'delete', $user->id],
                                                                [
                                                                    'confirm' => __('Are you sure you want to delete # {0}?', $user->id),
                                                                    'class' => 'btn btn-box-tool', 
                                                                    'escape' => false
                                                                ]) ?>
                                                        </td><?php
                                                    endif;?>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <?php echo $this->element('paginator'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>