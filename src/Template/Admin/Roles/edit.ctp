<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Roles'); 
$this->Breadcrumbs->add([
        ['title' => 'Roles', 'url' => ['controller' => 'roles', 'action' => 'index']],
        ['title' => 'Update Role']
    ]); 
?>
<div class="row">
    <div class="col-md-3 col-md-push-9 special-box">
        <!-- Acciones -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Navegación</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-book icon-min"></i> Volver',
                                ['controller' => 'roles', 'action' => 'index'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!--/.box -->
        <!-- /.box -->
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div class="box box-danger">
            <div class="box-header with-border">
                            <h3 class="box-title">Update Role</h3>
                        </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create($role) ?>
                            <div class="row">
                              <div class="col-md-4">
                                    <div class="form-group"><?php
                                        echo $this->Form->control('name', ['label' => 'name', 'placeholder' => 'name', 'class' => 'form-control']);?>
                                    </div>
                                </div>
                                                </div>
                            <?= $this->Form->button(__('Submit'),['class' => 'btn btn-danger']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>