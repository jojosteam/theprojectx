<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Roles'); 
$this->Breadcrumbs->add([
        ['title' => 'Roles', 'url' => ['controller' => 'roles', 'action' => 'index']],
        ['title' => 'Role information']
    ]); 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <!-- Header del contenedor -->
                <h3 class="box-title">Información Role</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                                                                                                                        <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Name') ?></label>
                                        <span><?= h($role->name) ?></span>
                                        </div>
                                    </div>
                                                                                                                                                                                                            <div class="col-md-4">
                                        <div class="group">
                                        <label class="center-block"><?= __('Id') ?></label>
                                        <span><?= $this->Number->format($role->id) ?></span>
                                        </div>
                                    </div>
                                                                                                                                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>