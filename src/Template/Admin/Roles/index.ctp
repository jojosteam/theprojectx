<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php 
$this->assign('content_title', 'Roles'); 
$this->Breadcrumbs->add([
        ['title' => 'Roles']
    ]);                
?>
<div class="row">
    <!-- Col de navegación -->
    <div class="col-md-3 col-md-push-9 special-box">
        <!--/.box -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> New Role',
                                ['action' => 'add'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> List Users',
                                ['controller' => 'Users', 'action' => 'index'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                        <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="glyphicon glyphicon-plus"></i> New User', 
                                ['controller' => 'Users', 'action' => 'add'], 
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                        </div>
                                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!--/.box -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Filtrar por</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <?= $this->Form->control('name',[
                                    'label' => 'name',
                                    'class' => 'form-control'])
                                ?>
                            </div>
                            <?= $this->Form->button('Filtrar', [
                                'type' => 'submit', 
                                'class' => 'btn btn-danger'])
                            ?>
                            <?= $this->Html->link('Limpiar', 
                                ['action' => 'index'],
                                ['class' => 'btn btn-default pull-right'])
                            ?>
                        <?= $this->Form->end() ?>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="roles index col-md-9 col-md-pull-3">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">List New Role</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover text-center">
                                        <tbody>
                                            <tr class="success">
                                                                                                <th scope="col"><?= $this->Paginator->sort('id', 'id') ?></th>
                                                                                <th scope="col"><?= $this->Paginator->sort('name', 'name') ?></th>
                                                                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                                            </tr>
                                            <?php foreach ($roles as $role): ?>
                                                <tr>
                                                                                        <td><?= $this->Number->format($role->id) ?></td>
                                                                                        <td><?= h($role->name) ?></td>
                                                                                        <td class="actions">
                                                        <?= $this->Html->link('<i class="fa fa-search"></i>', 
                                                            ['action' => 'view', $role->id],
                                                            ['class' => 'btn btn-box-tool', 'escape' => false]) 
                                                        ?>
                                                        <?= $this->Html->link('<i class="fa fa-pencil-square-o"></i>',
                                                            ['action' => 'edit', $role->id],
                                                            ['class' => 'btn btn-box-tool', 'escape' => false]) ?>
                                                        <?= $this->Form->postLink('<i  class="fa fa-trash"></i>', 
                                                            ['action' => 'delete', $role->id],
                                                            [
                                                                'confirm' => __('Are you sure you want to delete # {0}?', $role->id),
                                                                'class' => 'btn btn-box-tool', 
                                                                'escape' => false
                                                            ]) ?>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <?php echo $this->element('paginator'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>