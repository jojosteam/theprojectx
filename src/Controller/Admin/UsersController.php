<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index(){
        $conditions = [];
        
        if(!empty($this->request->query["rut"])){
            $conditions["rut LIKE"] = $this->request->query["rut"];
        }
        if(!empty($this->request->query["role_id"])){
            $conditions["role_id"] = $this->request->query["role_id"];
        }

        $query = $this->Users->find("all", [
            'contain' => ['Roles'],
            'conditions' => $conditions
        ]);
        $users = $this->paginate($query);
        $roles = $this->Users->Roles->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();

        $this->set(compact('users', 'roles'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($role)
    {
        $role = strtolower($role);
        if($role == 'medic'){
            $title = "Médico";
            $role_id = 5;
        }
        elseif($role == 'patient'){
            $title = "Paciente";
            $role_id = 4;
        }
        elseif($role == 'director'){
            $title = "Director";
            $role_id = 3;
        }
        elseif($role == 'secretarian'){
            $title = "Secretario/a";
            $role_id = 2;
        }
        else
            $this->Flash->error(__('Ha ocurrido un error. Por favor, contáctese con el administrador.'));

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->role_id = $role_id;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuario agregado correctamente.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('El usuario no pudo ser guardado. Por favor, intente nuevamente.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        if (strtolower($role) == 'medic') 
            $this->set('medic', true);
        $this->set(compact('user', 'roles', 'title'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login(){
        if ($this->isLoggedIn()){
            return $this->redirect($this->Auth->redirectUrl());
        }

        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $user = $this->Users->get($user["id"]);

                $this->Auth->setUser($user);
                
                return $this->redirect($this->Auth->redirectUrl());
            }
            else{
                $message = 'Usuario y/o contraseña incorrectos, por favor inténtelo nuevamente.';
            }

            $this->Flash->error($message);
        }
    }

    public function logout(){
        $this->Flash->success("Sesión cerrada correctamente.");
        return $this->redirect($this->Auth->logout());
    }

    public function statistics(){
        $users = $this->Users->find('all')->where(['role_id' => 4])->contain(['Attentions'])->toArray();

        if(!empty($this->request->data['age_range']))
            $age_range = $this->request->data['age_range'];
        if(!empty($this->request->data['gender']))
            $gender = $this->request->data['gender'];
        if(!empty($this->request->data['attentions_quantity']))
            $attentions_quantity = $this->request->data['attentions_quantity'];

        foreach($users as $key => $user){
            $birthdate = date_create($user->birthdate);
            $now = date_create(date('Y-m-d'));
            $age = date_diff($birthdate, $now);
            $age = intval($age->format('%y'));

            $users[$key]['age'] = $age;
            $users[$key]['attentions_quantity'] = count($user->attentions_quantity);

            if(isset($age_range)){
                switch ($age_range) {
                    case 1:
                        if($age < 0 || $age > 14){
                            unset($users[$key]);
                        }
                        break;
                    case 2: 
                        if($age < 14 || $age > 27){
                            unset($users[$key]);
                        }
                        break;
                    case 3:
                        if ($age < 27 || $age > 60) {
                            unset($users[$key]);
                        }
                        break;
                    case 4:
                        if($age < 60){
                            unset($users[$key]);
                        }
                        break;
                    default:
                        break;
                }
            }

            if(isset($gender)){
                if(intval($user->gender) != intval($gender)){
                    unset($users[$key]);
                }
            }

            if(isset($attentions_quantity)){
                if($attentions_quantity != count($user->attentions_quantity)){
                    unset($users[$key]);
                }
            }
        }

        $this->set(compact('users', 'roles'));
        $this->set('_serialize', ['users']);
    }
}
