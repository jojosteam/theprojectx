<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AttentionStates Controller
 *
 * @property \App\Model\Table\AttentionStatesTable $AttentionStates
 *
 * @method \App\Model\Entity\AttentionState[] paginate($object = null, array $settings = [])
 */
class AttentionStatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $attentionStates = $this->paginate($this->AttentionStates);

        $this->set(compact('attentionStates'));
        $this->set('_serialize', ['attentionStates']);
    }

    /**
     * View method
     *
     * @param string|null $id Attention State id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $attentionState = $this->AttentionStates->get($id, [
            'contain' => []
        ]);

        $this->set('attentionState', $attentionState);
        $this->set('_serialize', ['attentionState']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $attentionState = $this->AttentionStates->newEntity();
        if ($this->request->is('post')) {
            $attentionState = $this->AttentionStates->patchEntity($attentionState, $this->request->getData());
            if ($this->AttentionStates->save($attentionState)) {
                $this->Flash->success(__('The attention state has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attention state could not be saved. Please, try again.'));
        }
        $this->set(compact('attentionState'));
        $this->set('_serialize', ['attentionState']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Attention State id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $attentionState = $this->AttentionStates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attentionState = $this->AttentionStates->patchEntity($attentionState, $this->request->getData());
            if ($this->AttentionStates->save($attentionState)) {
                $this->Flash->success(__('The attention state has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attention state could not be saved. Please, try again.'));
        }
        $this->set(compact('attentionState'));
        $this->set('_serialize', ['attentionState']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Attention State id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attentionState = $this->AttentionStates->get($id);
        if ($this->AttentionStates->delete($attentionState)) {
            $this->Flash->success(__('The attention state has been deleted.'));
        } else {
            $this->Flash->error(__('The attention state could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
