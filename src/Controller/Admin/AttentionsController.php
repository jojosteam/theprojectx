<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Attentions Controller
 *
 * @property \App\Model\Table\AttentionsTable $Attentions
 *
 * @method \App\Model\Entity\Attention[] paginate($object = null, array $settings = [])
 */
class AttentionsController extends AppController
{

    public function initialize(){
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['statistics', 'index']
        ]);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index(){
        $currentUser = $this->getCurrentUser();

        $conditions = [];

        if($currentUser->role_id == 4){
            $conditions = [
                "patient_id" => $currentUser->id
            ];
        }

        $query = $this->Attentions->find('search',[
            'search' => $this->request->query,
            'conditions' => $conditions,
            'contain' => ['Patients', 'Medics', 'States']
        ]);

        $attentions = $this->paginate($query);
        $states = $this->Attentions->States->find('list');
        $medics = $this->Attentions->Medics->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);

        $this->set(compact('attentions', 'states', 'medics'));
    }

    /**
     * View method
     *
     * @param string|null $id Attention id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     *
    public function view($id = null)
    {
        $attention = $this->Attentions->get($id, [
            'contain' => ['Patients', 'Medics', 'States']
        ]);

        $this->set('attention', $attention);
        $this->set('_serialize', ['attention']);
    }*/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $attention = $this->Attentions->newEntity();
        if ($this->request->is('post')) {
            $attention = $this->Attentions->patchEntity($attention, $this->request->getData());
            if ($this->Attentions->save($attention)) {
                $this->Flash->success(__('La atención ha sido creada correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La atención no ha podido ser creada. Por favor, intételo nuevamente.'));
        }

        $patients = $this->Attentions->Patients->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);
        $medics = $this->Attentions->Medics->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);
        $states = $this->Attentions->States->find('list');
        $this->set(compact('attention', 'patients', 'medics', 'states'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Attention id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){
        $attention = $this->Attentions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attention = $this->Attentions->patchEntity($attention, $this->request->getData());
            if ($this->Attentions->save($attention)) {
                $this->Flash->success(__('La atención ha sido actualizada correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La atención no ha podido actualizar correctamente. Por favor, inténtelo nuevamente.'));
        }

        $patients = $this->Attentions->Patients->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);
        $medics = $this->Attentions->Medics->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);
        $states = $this->Attentions->States->find('list');
        $this->set(compact('attention', 'patients', 'medics', 'states'));
        $this->set('_serialize', ['attention']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Attention id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attention = $this->Attentions->get($id);
        if ($this->Attentions->delete($attention)) {
            $this->Flash->success(__('La atención ha sido eliminada correctamente.'));
        } else {
            $this->Flash->error(__('La atención no se ha podido eliminar. Por favor, inténtelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeState($attention_id, $state_id){
        $attention = $this->Attentions->get($attention_id);

        $attention->state_id = $state_id;

        if($this->Attentions->save($attention)){
            $this->Flash->success(__('El estado ha sido cambiado correctamente.'));
        }
        else{
            $this->Flash->error(__('El estado no se ha podido cambiar. Por favor, inténtelo nuevamente.'));
        }

        $this->redirect(["action" => "index"]);
    }

    public function statistics(){
        $attentions = $this->Attentions->find('search',['search' => $this->request->query]);
        $attentions
            ->contain(['Patients','Medics','States'])
            ->select(['total' => $attentions->func()->count('*'),'state_id','valorTotal' => $attentions->func()->sum('Medics.consult_price'),'medic_id','Medics.id','Medics.consult_price','States.name'])
            ->group('state_id');
        $medics = $this->Attentions->Medics->find('list', [
            'keyField' => 'id',
            'valueField' => 'fullname'
        ]);
        $states = $this->Attentions->States->find('list');
        $month = [1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'];
        $this->set(compact('attentions','states','medics','month'));
    }
}
