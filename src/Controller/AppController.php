<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller{
    private $currentUser = null;
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(){
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login',
                'prefix' => 'admin'
            ],
            'loginRedirect' => [
                'controller' => 'attentions',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login',
                'prefix' => 'admin'
            ],
            'authError' => 'No puedes acceder a esa sección.',
            'flash' => [
                'element' => 'error'
            ]
        ]);

        $this->Auth->config('authenticate', ['Form' => [
            'fields' => [
                'username' => 'rut',
                'password' => 'password'
            ],
            'userModel' => 'Users',
            'finder' => 'auth'
        ]]);

        /*$this->Auth->allow([
           '*',
        ]);*/
    }

    public function beforeRender(Event $event){
        if(isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin'){
            $this->viewBuilder()->setLayout('admin');

            Configure::load('adminSidebar');
            $this->set('sidebar', Configure::read('Sidebar'));
            $adminPublicActions = array('login');

            if (in_array($this->request->params['action'], $adminPublicActions)){
                $this->viewBuilder()->setLayout('login');
            }
        }
    }

    public function beforeFilter(Event $event){
        if ($this->Auth->user() !== null){
            $this->set('currentUser', $this->Auth->user());
            $this->currentUser = $this->Auth->user();
        }
    }

    protected function isLoggedIn(){
        return ($this->currentUser !== null);
    }

    protected function getCurrentUser(){
        return $this->currentUser;
    }
}
