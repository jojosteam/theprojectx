<?php
$config = [
    'Sidebar' => [
        /*[
            'name' => 'Default', # nombre
            'ico' => false, # ícono (se obtiene desde font-awesome)
            'current' => false, #Si es que tiene un submenu
            'current' => [ # current se usa para agregar la clase "active" a la pestaña escogida y la url respectiva
                'controller' => false, # controller actual
                'action' => false # action del controller actual
            ]
            'submenu' => false, # si no tiene submenú dejar en false
            'submenu' => [
                [
                    'name' => '', # nombre a desplegar en submenú,
                    'current' => false, #Si es que no tiene submenu
                    'current' => [ # current se usa para la clase active
                        'action' => '', # vista de submenú
                        'controller' => '', # controller de submenú
                    ],
                    'submenu' => [
                        'name' => '', # nombre a desplegar en submenú,
                        'current' => [ # current se usa para la clase active
                            'action' => '', # vista de submenú
                            'controller' => '', # controller de submenú
                        ],
                        ...
                    ],
                    ...
                ],
                ...
            ]
        ],*/
        [
            'name' => 'Atenciones',
            'ico' => 'fa-calendar',
            'current' => false,
            'submenu' => [
                [
                    'name' => 'Listado',
                    'current' => ['action' => 'index','controller' => 'Attentions']
                ],
                [
                    'name' => 'Agregar',
                    'current' => ['action' => 'add','controller' => 'Attentions']
                ],
                [
                    'name' => 'Estadísticas de atenciones',
                    'current' => ['action' => 'statistics','controller' => 'Attentions']
                ],
            ]
        ],
        [
            'name' => 'Usuarios',
            'ico' => 'fa-users',
            'current' => false,
            'submenu' => [
                [
                    'name' => 'Listado',
                    'current' => ['action' => 'index','controller' => 'Users']
                ],
                [
                    'name' => 'Agregar médico',
                    'current' => ['action' => 'add','controller' => 'Users', 'medic']
                ],
                [
                    'name' => 'Agregar paciente',
                    'current' => ['action' => 'add','controller' => 'Users', 'patient']
                ],
                [
                    'name' => 'Agregar secretaria',
                    'current' => ['action' => 'add','controller' => 'Users', 'secretarian']
                ],
                [
                    'name' => 'Agregar director',
                    'current' => ['action' => 'add','controller' => 'Users', 'director']
                ],
                [
                    'name' => 'Estadísticas de personas',
                    'current' => ['action' => 'statistics','controller' => 'Users']
                ],
            ]
        ],
    ]
];